let row = 20;

for (let i = 0; i < row; i++) {
    // create row
    for (let j = 0; j < row; j++) {
        const isEdge = !(i !== 0 && i !== row - 1 && j !== 0 && j !== row - 1);
        if (isEdge) {
           process.stdout.write('* ');
        } else {
           process.stdout.write('  ');
        }
    }
   process.stdout.write('\n');
}