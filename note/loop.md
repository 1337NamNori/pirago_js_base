## For loop
`for ([initialization];[condition];[final-expression]){`\
`//   Block of code`\
`}`
# While loop
`while (condition) {`\
`// Block of code`\
`}`
# Do ... While
`do {`\
`//    Block of code`\
`}`\
`while (condition);`
# For ... in 
`for (variableName in object) {`\
`//    Block of code`\
`}`
# For ... of
`for (variable of iterable) {`\
`//  Block of code`\
`}`