# Sort
- sort()
- reverse()\

`array.sort((a, b) => {`\
    `return a < b;`\
`});`
# Other 
- forEach()
- find()
- filter()
- some()
- every()
- push()
- pop()
- shift()
- unshift()
- map()
- reduce()

