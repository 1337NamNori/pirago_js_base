- câu lệnh try để chạy đoạn code có thể phát sinh lỗi
- câu lệnh catch để bắt và xử lý lỗi
- câu lệnh throw để custom lỗi
- câu lệnh finally để excute code, bất kể có lỗi hay không
# Try and catch
`try {`\
`//  Block of code to try`
`}`\
`catch(err) {`\
`//  Block of code to handle errors`\
`}`
# Throw error
`throw error`
# Finally
`try {`\
`//  Block of code to try`\
`}`\
`catch(err) {`\
`//  Block of code to handle errors`\
`}`\
`finally {`\
`//  Block of code to be executed regardless of the try / catch result`\
`}`
# Error Object
## Error Object Properties
- name
- message
## Error Name Value
- EvalError 
- RangeError 
- ReferenceError
- SyntaxError
- TypeError
- URIError
