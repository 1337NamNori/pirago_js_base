# Arithmetic Operators
- \+
- \-
- \*
- \\
- %
- ++
- --

# Assignment Operators
- =
- \+=
- \-=
- \*=
- \\=
- %=

# String Operators
có thể dùng + và +=

# Adding Strings and Numbers
`let x = 5 + 5; `//10\
`let y = "5" + 5; `//55\
`let z = "Hello" + 5; `//Hello5

# Comparison Operators
- ==
- ===
- !=
- !==
- \>
- \<
- \>=
- \<=
- ?

# Logical Operators
- &&
- ||
- !

# Type Operators
- typeof
- instanceof

# Bitwise Operators
- & AND
- | OR
- ~ NOT
- ^ XOR
- \<< Zero fill left shift
- \>> Signed right shift
- \>>> Zero fill right shift




