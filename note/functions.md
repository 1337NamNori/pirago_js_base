`function name(parameter1, parameter2, parameter3) {`\
`  // code to be executed`\
`}`
# Return
- khi chạy đến câu lệnh `return`, hàm sẽ dừng chạy
- giá trị được return sẽ được gán cho caller
# Why Function?
- để dùng lại code trong khi chỉ cần định nghĩa 1 lần
- dùng lại một đoạn code với những tham số khác, để có được những kết quả trả về khác
# Local variables
- biến được khai báo trong cặp đóng mở ngoặc nhọn {} là biến local, chỉ có thể sử dụng trong hàm.
